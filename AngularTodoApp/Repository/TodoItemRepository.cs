﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AngularTodoApp.Context;
using Microsoft.EntityFrameworkCore.Query;
using AngularTodoApp.Model;
using AngularTodoApp.Extensions;

namespace AngularTodoApp.Repository
{
    public class TodoItemRepository : BaseRepository<Todo>
    {
        public TodoItemRepository(DbContext context) : base(context)
        {
        }

        private Expression<Func<Todo, bool>> GetStatusWhere(StatusEnum statusEnum)
        {
            var id = (int)statusEnum;
            switch (statusEnum)
            {
                default:
                    return p => true;
                case StatusEnum.Pending:
                case StatusEnum.Completed:
                case StatusEnum.InProgress:
                case StatusEnum.Close:
                    return p => p.Status == id;

            }
        }
        private Expression<Func<Todo, bool>> GetSearchWhere(string search , SearchTodo searchTodo)
        {
            if (!string.IsNullOrEmpty(search))
            {
                switch (searchTodo)
                {
                    case SearchTodo.Name:
                        return p => (p.Title.Contains(search));
                    case SearchTodo.CreadtedAt:
                        return p => (p.CreatedAt.ToString("G").Contains(search));
                    case SearchTodo.AssignTo:
                        return p => (p.AssignedUser.FullName.ToLower().Contains(search));
                    default:
                        return p => (p.Title.ToLower().Contains(search) || p.CreatedAt.ToString("G").Contains(search) || p.AssignedUser.FullName.ToLower().Contains(search));
                }
            }
            return null;
        }
        private Expression<Func<Todo, object>> GetOrder(OrderByTodo orderBy)
        {
            switch (orderBy)
            {

                case OrderByTodo.AssignTo:
                    return p => p.AssignedUser.FullName;
                case OrderByTodo.Status:
                    return p => p.Status;
                case OrderByTodo.CreatedAt:
                    return p => p.CreatedAt;
                case OrderByTodo.Name:
                    return p => p.Title;
                default:
                    return null;

            }
        }

        public int SearchCount(string userId,string search, SearchTodo searchTodo = SearchTodo.Any, StatusEnum statusEnum = StatusEnum.Any,int pageSize = 10)
        {
            var mysearch = GetSearchWhere(search, searchTodo);
            mysearch = mysearch == null ? GetStatusWhere(statusEnum) : mysearch.And(GetStatusWhere(statusEnum));
            mysearch = mysearch.And(p => p.UserId == userId);
            return PageCount(mysearch, pageSize);
        }
        public IQueryable<Todo> Search(string userId, string search = "",SearchTodo searchTodo = SearchTodo.Any, StatusEnum statusEnum = StatusEnum.Any, OrderByTodo orderBy = OrderByTodo.None, int page = 1,
            int pageSize = 10, bool isDescending = false,
            Func<IQueryable<Todo>, IIncludableQueryable<Todo, object>> include = null)
        {

            var mysearch = GetSearchWhere(search, searchTodo);
            mysearch = mysearch == null ? GetStatusWhere(statusEnum) : mysearch.And(GetStatusWhere(statusEnum));
            mysearch = mysearch.And(p => p.UserId == userId);
            var myorder = GetOrder(orderBy);
            return GetAll(mysearch, myorder, page, pageSize, isDescending, include);
        }
    }
}

