﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query;
using AngularTodoApp.Model;
namespace AngularTodoApp.Repository
{
    public interface IRepository<T> where T : class 
    {

        IQueryable<T> GetAll(Expression<Func<T, bool>> where = null,Expression<Func<T, object>> orderby = null, int page = 1, int pageSize = 10,bool isDescending = false,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        IQueryable<T> GetMany(Expression<Func<T, bool>> where, Expression<Func<T, object>> orderby = null, int page = 1, int pageSize = 10, bool isDescending = false,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        T GetBy(Expression<Func<T, bool>> where,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        T GetById(int id,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        int PageCount(Expression<Func<T, bool>> where = null, int pageSize = 10);

        void Add(T entity);
        void Update(T entity);
        bool Delete(Expression<Func<T, bool>> where);
        void Delete(T entity);
        bool SaveChanges();
        Task<bool> SaveChangesAsync();

    }
}
