﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using AngularTodoApp.Model;
using AngularTodoApp.Extensions;

namespace AngularTodoApp.Repository
{
    public class BaseRepository<T> : IRepository<T> where T: Base 
    {
        private readonly DbContext _context;

        public BaseRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> where = null,Expression<Func<T, object>> @orderby = null, int page = 1, int pageSize = 10, bool isDescending = false,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            if (where != null)
                return GetMany(where, orderby, page, pageSize, isDescending, include);
            var query = (IQueryable<T>)_context.Set<T>();
            if (include != null)
            {
                query = include(query);
            }
            if (orderby != null)
            {
                query = isDescending ? query.OrderByDescending(@orderby) : query.OrderBy(@orderby);
            }
            else
            {
                //query = isDescending ? query.OrderByDescending(p => p.Id) : query.OrderBy(p => p.Id);
            }

            return query.Pagination(page, pageSize);
        }

        public IQueryable<T> GetMany(Expression<Func<T, bool>> @where, Expression<Func<T, object>> @orderby = null, int page = 1, int pageSize = 10,
            bool isDescending = false, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            var query = _context.Set<T>().Where(where);
            if (include != null)
            {
                query = include(query);
            }
            if (orderby != null)
            {
                query = isDescending ? query.OrderByDescending(@orderby) : query.OrderBy(@orderby);
            }
            else
            {
                query = isDescending ? query.OrderByDescending(p => p.Id) : query.OrderBy(p => p.Id);
            }

            return query.Pagination(page, pageSize);
        }

        public T GetBy(Expression<Func<T, bool>> @where, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            var query = _context.Set<T>().Where(where);
            if (include != null)
            {
                query = include(query);
            }
            return query.FirstOrDefault();
        }

        public T GetById(int id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            var query = _context.Set<T>().Where(p => p.Id == id);
            if (include != null)
            {
                query = include(query);
            }
            return query.FirstOrDefault();
        }

        public int PageCount(Expression<Func<T, bool>> @where = null, int pageSize = 10)
        {
            var query = (IQueryable<T>)_context.Set<T>();
            if (where != null)
            {
                query = query.Where(where);
            }
            return query.PageCount(pageSize);
        }

        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public bool Delete(Expression<Func<T, bool>> @where)
        {
            var entity = _context.Set<T>().FirstOrDefault(where);
            if (entity != null)
            {
                Delete(entity);
                return true;
            }
            return false;
        }

        public void Delete(T entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() == 1;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() == 1;
        }
    }
}
