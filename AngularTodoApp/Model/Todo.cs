﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTodoApp.Model
{
    public class Todo : Base
    {
        [Required]
        public string Title { get; set; }
        public int Status { get; set; }
        [Required]
        public string UserId { get; set; }
        [ForeignKey(nameof(UserId))]
        public User User { get; set; }
        public string AssignToId { get; set; }
        [ForeignKey(nameof(AssignToId))]
        public User AssignedUser { get; set; }
        public bool IsClosed { get; set; }
        public DateTime? ClosedDate { get; set; }
    }

    public enum StatusEnum
    {
        Any = 0,
        Pending = 1,
        Completed = 2,
        InProgress = 3,
        Close = 4,
    }

    public enum OrderByTodo
    {
        None = 0,
        Status = 1,
        CreatedAt = 2,
        Name = 3,
        AssignTo = 4,
    }
    public enum SearchTodo
    {
        Any = 0,
        Name = 1,
        CreadtedAt = 2,
        AssignTo = 3,
    }

}
