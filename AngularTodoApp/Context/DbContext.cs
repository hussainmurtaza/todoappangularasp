﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTodoApp.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AngularTodoApp.Context
{
    public class DbContext : IdentityDbContext<User>
    {
        public DbContext(DbContextOptions options)
            : base(options)
        {

        }
        public DbSet<Todo> Todos { get; set; }
    }
}
