﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace AngularTodoApp.Extensions
{
    public static class EfExtentions
    {
        public static IQueryable<T> IncludeMultiple<T>(this IQueryable<T> query, params Expression<Func<T, object>>[] includes)
            where T : class
        {
            if (includes != null)
            {
                query = includes.Aggregate(query,
                    (current, include) => current.Include(include));
            }
            return query;
        }


        public static IQueryable<T> Pagination<T>(this IQueryable<T> query, int page,int pageSize) where T : class 
        {
            if (page < 1) page = 1;
            if (pageSize < 1) pageSize = 10;
            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }


        public static int PageCount<T>(this IQueryable<T> query, int pageSize) where T : class
        {
            if (pageSize < 1) pageSize = 10;
            return (int)Math.Ceiling((float)query.Count() / pageSize);
        }


    }
}
