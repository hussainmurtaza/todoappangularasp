﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AngularTodoApp.Extensions
{
    public static class ClaimExtension
    {
        public static string UserId(this ClaimsPrincipal user)
        {
            var userId = user.Claims.FirstOrDefault(p => p.Type == "Id")?.Value;
            return userId;
        }
    }
}
