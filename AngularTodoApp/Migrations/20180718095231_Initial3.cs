﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AngularTodoApp.Migrations
{
    public partial class Initial3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssignToId",
                table: "Todos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Todos_AssignToId",
                table: "Todos",
                column: "AssignToId");

            migrationBuilder.AddForeignKey(
                name: "FK_Todos_AspNetUsers_AssignToId",
                table: "Todos",
                column: "AssignToId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Todos_AspNetUsers_AssignToId",
                table: "Todos");

            migrationBuilder.DropIndex(
                name: "IX_Todos_AssignToId",
                table: "Todos");

            migrationBuilder.DropColumn(
                name: "AssignToId",
                table: "Todos");
        }
    }
}
