﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AngularTodoApp.Model;

namespace AngularTodoApp.ViewModel
{
    public class TodoItemViewModel
    {
        public int Id { get; set; }
        [Required, MaxLength(100, ErrorMessage = "Cannot be more than 100 character")]
        [RegularExpression(@"^[a-zA-Z0-9\s]{1,}", ErrorMessage = "No Special Characters Allowed")]
        public string Title { get; set; }
        [Range(1, 3)]
        public int Status { get; set; }
    }

    public class TodoItemAssignViewModel
    {
        [Required]
        public string AssignUserId { get; set; }
    }
}
