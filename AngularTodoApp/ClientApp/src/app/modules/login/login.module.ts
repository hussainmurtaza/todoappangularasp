import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LogInComponent} from '../../components/log-in/log-in.component';
import {RegisterComponent} from '../../components/register/register.component';
import {LoginRoutingModule} from '../../routes/login-routing/login-routing.module';
import {HomeComponent} from '../../components/home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ForgetPasswordComponent} from '../../components/forget-password/forget-password.component';
import {ResetPasswordComponent} from '../../components/reset-password/reset-password.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [LogInComponent, RegisterComponent, HomeComponent, ForgetPasswordComponent, ResetPasswordComponent]
})
export class LoginModule { }
