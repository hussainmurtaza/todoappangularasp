import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const status = value as number;
    if (status === 1) {
      return 'Pending';
    } else if (status === 2) {
      return 'Completed';
    } else if (status === 3) {
      return 'In Progress';
    } else if (status === 4) {
      return 'Closed';
    }
    return value;
  }

}
