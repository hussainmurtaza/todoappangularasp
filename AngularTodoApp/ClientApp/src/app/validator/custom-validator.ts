import {FormGroup} from '@angular/forms';

export class RegistrationValidator {
  static validate(registrationFormGroup: FormGroup) {
    const password = registrationFormGroup.controls.password.value;
    const repeatPassword = registrationFormGroup.controls.confirmPassword.value;
    if (repeatPassword.length <= 0) {
      return null;
    }
    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }

    return null;

  }
}
export class ResetValidator {
  static validate(registrationFormGroup: FormGroup) {
    const password = registrationFormGroup.controls.newPassword.value;
    const repeatPassword = registrationFormGroup.controls.newRepeatPassword.value;
    if (repeatPassword.length <= 0) {
      return null;
    }
    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }

    return null;

  }
}

export class ChangeValidator {
  static validate(registrationFormGroup: FormGroup) {
    const password = registrationFormGroup.controls.newPassword.value;
    const repeatPassword = registrationFormGroup.controls.newRepeatPassword.value;
    if (repeatPassword.length <= 0) {
      return null;
    }
    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }

    return null;

  }
}
