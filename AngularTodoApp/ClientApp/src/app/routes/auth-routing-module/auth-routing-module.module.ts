import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {CreateTodoItemComponent} from '../../components/create-todo-item/create-todo-item.component';
import {MyTaskComponent} from '../../components/my-task/my-task.component';
import {DashboardComponent} from '../../components/dashboard/dashboard.component';
import {ActivateService} from '../../services/activate.service';
import {ChangePasswordComponent} from '../../components/change-password/change-password.component';

const routes: Routes = [
  { path: '', component: DashboardComponent, pathMatch: 'full', canActivate: [ ActivateService ] },
  { path: 'create', component: CreateTodoItemComponent, pathMatch: 'full', canActivate: [ ActivateService ] },
  { path: 'tasks', component: MyTaskComponent, pathMatch: 'full', canActivate: [ ActivateService ] },
  { path: 'changePassword', component: ChangePasswordComponent, pathMatch: 'full', canActivate: [ ActivateService ] },
];
@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule],
  declarations: []
})
export class AuthRoutingModuleModule { }
