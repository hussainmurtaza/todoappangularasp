import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from '../../components/home/home.component';
import {RegisterComponent} from '../../components/register/register.component';
import {LogInComponent} from '../../components/log-in/log-in.component';
import {ForgetPasswordComponent} from '../../components/forget-password/forget-password.component';
import {ResetPasswordComponent} from '../../components/reset-password/reset-password.component';
const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'login', component: LogInComponent, pathMatch: 'full' },
  { path: 'register', component: RegisterComponent, pathMatch: 'full'},
  { path: 'forgetPassword', component: ForgetPasswordComponent, pathMatch: 'full'},
  { path: 'resetPassword', component: ResetPasswordComponent, pathMatch: 'full'},
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: []
})
export class LoginRoutingModule { }
