export class Api {
  public static USER_CREATE = 'api/Auth/register';
  public static USER_LOGIN = 'api/Auth/login';
  public static USER_FORGET_PASSWORD = 'api/Auth/forgetPassword';
  public static USER_RESET_PASSWORD = 'api/Auth/resetPassword';
  public static USER_CHANGE_PASSWORD = 'api/Auth/changePassword';
  public static USER_GET_ALL = 'api/User/getAll';
  public static TODO_GETALL = 'api/Todo/getAll';
  public static TODO_GET_ALL_ASSIGN = 'api/Todo/getAssign';
  public static TODO_GET = 'api/Todo/get';
  public static TODO_SEARCH = 'api/Todo/search';
  public static TODO_CREATE = 'api/Todo/create';
  public static TODO_UPDATE = 'api/Todo/update';
  public static TODO_ASSIGN = 'api/Todo/assign';
  public static TODO_CLOSE = 'api/Todo/close';
  public static TODO_DELETE = 'api/Todo/delete';
}
