import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { AlertComponent } from './components/alert/alert.component';
import {AlertService} from './services/alert.service';
import {MyHttpInterceptor} from './components/interceptor/my-http-interceptor';
import { DeleteTodoItemComponent } from './components/modals/delete-todo-item/delete-todo-item.component';
import { EditTodoItemComponent } from './components/modals/edit-todo-item/edit-todo-item.component';
import { AssignTodoItemComponent } from './components/modals/assign-todo-item/assign-todo-item.component';
import { CloseTodoItemComponent } from './components/modals/close-todo-item/close-todo-item.component';
import { DashboardNavMenuComponent } from './components/dashboard-nav-menu/dashboard-nav-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    AlertComponent,
    DeleteTodoItemComponent,
    EditTodoItemComponent,
    AssignTodoItemComponent,
    CloseTodoItemComponent,
    DashboardNavMenuComponent,
  ],
  entryComponents: [CloseTodoItemComponent, DeleteTodoItemComponent, EditTodoItemComponent, AssignTodoItemComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    CollapseModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'account',
        loadChildren: 'app/modules/login/login.module#LoginModule'
      },
      {
        path: 'dashboard',
        loadChildren: 'app/modules/auth/auth.module#AuthModule'
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }
    ])
  ],
  providers: [AlertService, {  provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
