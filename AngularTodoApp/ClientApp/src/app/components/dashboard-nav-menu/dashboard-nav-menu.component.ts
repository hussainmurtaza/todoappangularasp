import { Component, OnInit } from '@angular/core';
import {StorageService} from '../../services/storage.service';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard-nav-menu',
  templateUrl: './dashboard-nav-menu.component.html',
  styleUrls: ['./dashboard-nav-menu.component.css']
})
export class DashboardNavMenuComponent implements OnInit {

  constructor(private router: Router, private storage: StorageService, public service: UserService) { }

  ngOnInit() {
  }

  LogOut(): void {
    this.storage.clear();
    this.router.navigateByUrl('/account/login');
  }
}
