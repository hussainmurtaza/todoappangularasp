import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Token} from '../../resultModel/token';
import {ResponseResult} from '../../resultModel/responseResult';
import {UtilResponse} from '../../util/util-response';
import {User} from '../../model/user';
import {UserService} from '../../services/user.service';
import {AlertService} from '../../services/alert.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
})
export class ForgetPasswordComponent implements OnInit {

  loginForm: FormGroup;
  email: FormControl;
  constructor(private alert: AlertService, private service: UserService) { }

  ngOnInit() {
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.loginForm = new FormGroup({
      email: this.email,
    });
  }
  onSubmit(form: FormGroup) {
    if (form.valid) {
      const u = new User();
      u.email = this.email.value;

      const response = this.service.forgetPassword(u.email);
      response.subscribe(
        result => {
          const r = result as ResponseResult;
          if (r.message === UtilResponse.SUCCESS) {
            this.alert.success('Sent Email with Reset Link');
          } else if (r.message === UtilResponse.BAD || r.message === UtilResponse.ERROR) {
            r.error.map( p => {
              this.alert.warn(p);
            });
          }
        }, error => {
          console.error(error);
          this.alert.error('Error Occurred');
        }
      );
    }
  }

}
