import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../../services/alert.service';
import {BsModalRef} from 'ngx-bootstrap';
import {TodoService} from '../../../services/todo.service';
import {ResponseResult} from '../../../resultModel/responseResult';
import {UtilResponse} from '../../../util/util-response';
import {Todo} from '../../../model/todo';

@Component({
  selector: 'app-close-todo-item',
  templateUrl: './close-todo-item.component.html',
})
export class CloseTodoItemComponent implements OnInit {
  id: number;
  constructor(public bsModalRef: BsModalRef, private service: TodoService, private alert: AlertService) { }

  ngOnInit() {
  }
  closeItem(): void {
    this.service.Close(this.id).subscribe(result => {
      const data = result as ResponseResult;
      if (data.message === UtilResponse.SUCCESS) {
        this.alert.success('Closed');
        const it = data.result as Todo;
        const index = this.service.list.findIndex(x => x.id === this.id);
        if (index > -1) {
          this.service.list[index].status = it.status;
          this.service.list[index].isClosed = it.isClosed;
          this.service.list[index].closedDate = it.closedDate;
        }
        this.bsModalRef.hide();
      } else if (data.message === UtilResponse.BAD || data.message === UtilResponse.ERROR) {
        this.alert.error('Error Closing Task');
      }
    }, ex => { console.error(ex); });
  }
}
