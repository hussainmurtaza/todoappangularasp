import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {TodoService} from '../../../services/todo.service';
import {ResponseResult} from '../../../resultModel/responseResult';
import {UtilResponse} from '../../../util/util-response';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-delete-todo-item',
  templateUrl: './delete-todo-item.component.html',
})
export class DeleteTodoItemComponent implements OnInit {


  id: number;
  constructor(public bsModalRef: BsModalRef, private service: TodoService, private alert: AlertService) { }

  ngOnInit() {
  }

  delete(): void {
    this.service.Delete(this.id).subscribe(result => {
      const data = result as ResponseResult;
      if (data.message === UtilResponse.SUCCESS) {
        this.alert.success('Deleted');
        const index = this.service.list.findIndex(x => x.id === this.id);
        if (index > -1) {
          this.service.list.splice(index, 1);
        }
        this.bsModalRef.hide();
      } else if (data.message === UtilResponse.BAD || data.message === UtilResponse.ERROR) {
        this.alert.error('Error Deleting');
      }
    }, ex => { console.error(ex); });
  }

}
