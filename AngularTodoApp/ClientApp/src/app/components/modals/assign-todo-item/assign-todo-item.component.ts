import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../model/user';
import {UserService} from '../../../services/user.service';
import {ResponseResult} from '../../../resultModel/responseResult';
import {UtilResponse} from '../../../util/util-response';
import {Todo} from '../../../model/todo';
import {BsModalRef} from 'ngx-bootstrap';
import {TodoService} from '../../../services/todo.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-assign-todo-item',
  templateUrl: './assign-todo-item.component.html',
})
export class AssignTodoItemComponent implements OnInit {

  todo: Todo;
  list: User[];
  createForm: FormGroup;
  assignControl: FormControl;
  constructor(private service: UserService, public bsModalRef: BsModalRef, private todoService: TodoService, private alert: AlertService) { }

  ngOnInit() {
    this.assignControl = new FormControl('', [Validators.required]);
    this.createForm = new FormGroup({
      assign: this.assignControl
    });
    this.service.GetAllUsers().subscribe(result => {
      const data = result as ResponseResult;
      if (data.message === UtilResponse.SUCCESS) {
        this.list = data.result as User[];
        this.assignControl.setValue(this.todo.assignToId);
      }
    }, er => { console.log(er); });

  }

  assign(form: FormGroup): void {
    if (form.valid) {
      const id = this.assignControl.value;
      this.todoService.Assign(this.todo.id, id).subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.bsModalRef.hide();
          const index = this.todoService.list.findIndex(x => x.id === this.todo.id);
          if (index > -1) {
            const it = data.result as Todo;
            this.todoService.list[index].title = it.title;
            this.todoService.list[index].status = it.status;
            this.todoService.list[index].createdAt = it.createdAt;
            this.todoService.list[index].updatedAt = it.updatedAt;
            const index2 = this.list.findIndex(x => x.id === id);
            if (index2 > -1) {
              this.todoService.list[index].assignedUser = this.list[index2];
            }
            this.alert.success('Task Assigned');
          }
        } else if (data.message === UtilResponse.ERROR || data.message === UtilResponse.BAD) {
          data.error.forEach(p => {
            this.alert.error(p);
          });
        }

      }, er => { console.error(er); });
    }
  }

}
