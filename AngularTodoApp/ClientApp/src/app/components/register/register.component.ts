import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RegistrationValidator} from '../../validator/custom-validator';
import {UserService} from '../../services/user.service';
import {User} from '../../model/user';
import {UtilResponse} from '../../util/util-response';
import {StorageService} from '../../services/storage.service';
import {AlertService} from '../../services/alert.service';
import {Router} from '@angular/router';
import {ResponseResult} from '../../resultModel/responseResult';
import {Token} from '../../resultModel/token';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  passwordFormGroup: FormGroup;
  email: FormControl;
  fullName: FormControl;
  password: FormControl;
  confirmPassword: FormControl;
  constructor(private router: Router, private service: UserService, private storage: StorageService, private alert: AlertService) {

  }

  ngOnInit() {
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.fullName = new FormControl('', [Validators.required]);
    this.password = new FormControl('', [Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.confirmPassword = new FormControl('', [Validators.required]);
    this.passwordFormGroup = new FormGroup({
      password: this.password,
      confirmPassword: this.confirmPassword
    }, RegistrationValidator.validate.bind(this));
    this.registerForm = new FormGroup({
      fullName: this.fullName,
      email: this.email,
      passwordFormGroup: this.passwordFormGroup
    });
  }

  onSubmit(form: FormGroup) {
    if (form.valid) {
      const u = new User();
      u.fullName = this.fullName.value;
      u.email = this.email.value;
      u.password = this.password.value;
      u.confirmPassword = this.confirmPassword.value;

      const response = this.service.registerUser(u);
      response.subscribe(
        result => {
          const r = result as ResponseResult;
          if (r.message === UtilResponse.SUCCESS) {
            this.alert.success('User Registered - Please wait for Email Verification Link');
          } else if (r.message === UtilResponse.BAD || r.message === UtilResponse.ERROR) {
            r.error.map( p => {
              this.alert.warn(p);
            });
          }
        }, error => {
          console.error(error);
          this.alert.error('Error Occurred');
        }
      );
    }

  }

}
