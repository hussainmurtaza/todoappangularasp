import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilResponse} from '../../util/util-response';
import {User} from '../../model/user';
import {UserService} from '../../services/user.service';
import {AlertService} from '../../services/alert.service';
import {StorageService} from '../../services/storage.service';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {ResponseResult} from '../../resultModel/responseResult';
import {Token} from '../../resultModel/token';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  // Validators.pattern('^[a-zA-Z0-9\\s]{1,}')
  loginForm: FormGroup;
  email: FormControl;
  password: FormControl;
  rememberMe: FormControl;
  isRemember = false;
  constructor(private route: ActivatedRoute, private router: Router, private service: UserService, private alert: AlertService, private storage: StorageService) {

  }

  ngOnInit() {
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [Validators.required, Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.rememberMe = new FormControl('');
    this.loginForm = new FormGroup({
      email: this.email,
      password: this.password,
      rememberMe: this.rememberMe
    });

    if (this.route.snapshot.queryParams['result'] === UtilResponse.SUCCESS) {
      this.alert.success('Email Verified');
    } else if (this.route.snapshot.queryParams['result'] === UtilResponse.ERROR) {
      this.alert.warn('Email Not Verified');
    }
  }
  onSubmit(form: FormGroup) {
    if (form.valid) {
      const u = new User();
      u.email = this.email.value;
      u.password = this.password.value;

      const response = this.service.loginUser(u, this.rememberMe.value.toLocaleString().length > 0);
      response.subscribe(
        result => {
          const r = result as ResponseResult;
          if (r.message === UtilResponse.SUCCESS) {
            this.alert.success('User Logged In');
            this.storage.setToken(r.result as Token);
            this.service.authenticated = true;
            this.service.expiryDate = (r.result as Token).endDate;
            this.service.isAuthenticated();
            this.router.navigateByUrl('').then(p => {
              console.log(this.service.expiryDate);
            }, er => {
              console.error(er);
            });
          } else if (r.message === UtilResponse.BAD || r.message === UtilResponse.ERROR) {
            r.error.map( p => {
              this.alert.warn(p);
            });
          }
        }, error => {
          console.error(error);
          this.alert.error('Error Occurred');
        }
      );
    }
  }

}
