import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ResetValidator} from '../../validator/custom-validator';
import {UserService} from '../../services/user.service';
import {AlertService} from '../../services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ResponseResult} from '../../resultModel/responseResult';
import {UtilResponse} from '../../util/util-response';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent implements OnInit {


  resetForm: FormGroup;
  email: FormControl;
  code: FormControl;
  userId: FormControl;
  passwordFormGroup: FormGroup;
  newPassword: FormControl;
  newRepeatPassword: FormControl;
  constructor(private router: Router, private service: UserService, private alert: AlertService, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.route.snapshot.queryParams['code'] === undefined || this.route.snapshot.queryParams['userId'] === undefined) {
      this.router.navigateByUrl('/account/login');
      return;
    }
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.code = new FormControl('', [Validators.required]);
    this.userId = new FormControl('', [Validators.required]);
    this.newPassword = new FormControl('', [Validators.required, Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.newRepeatPassword = new FormControl('', [Validators.required, Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.passwordFormGroup = new FormGroup({
      newPassword: this.newPassword,
      newRepeatPassword: this.newRepeatPassword
    }, ResetValidator.validate.bind(this));
    this.resetForm = new FormGroup({
      email: this.email,
      code: this.code,
      userId: this.userId,
      passwordFormGroup: this.passwordFormGroup
    });
    this.code.setValue(this.route.snapshot.queryParams['code']);
    this.userId.setValue(this.route.snapshot.queryParams['userId']);
  }

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      const response = this.service.resetPassword(this.email.value, this.code.value, this.userId.value, this.newPassword.value, this.newRepeatPassword.value);
      response.subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.alert.success('Password Updated');
          setTimeout(() => {
            this.router.navigateByUrl('/account/login');
          }, 5000);
        } else if (data.message === UtilResponse.ERROR || data.message === UtilResponse.BAD) {
            data.error.forEach(p => {
              this.alert.warn(p);
            });
        }
      });
    }
  }
}
