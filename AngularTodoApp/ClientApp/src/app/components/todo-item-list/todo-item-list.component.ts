import {Component, Input, OnInit} from '@angular/core';
import {Todo} from '../../model/todo';
import {TodoService} from '../../services/todo.service';
import {StorageService} from '../../services/storage.service';
import {UtilResponse} from '../../util/util-response';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {DeleteTodoItemComponent} from '../modals/delete-todo-item/delete-todo-item.component';
import {ResponseResult} from '../../resultModel/responseResult';
import {EditTodoItemComponent} from '../modals/edit-todo-item/edit-todo-item.component';
import {AssignTodoItemComponent} from '../modals/assign-todo-item/assign-todo-item.component';
import {CloseTodoItemComponent} from '../modals/close-todo-item/close-todo-item.component';

@Component({
  selector: 'app-todo-item-list',
  templateUrl: './todo-item-list.component.html',
  styleUrls: ['./todo-item-list.component.css']
})
export class TodoItemListComponent implements OnInit {


  @Input() isSelf = false;
  bsModalRef: BsModalRef;

  constructor(public service: TodoService, private storage: StorageService, private modalService: BsModalService) { }

  ngOnInit() {
    this.service.currentPage = 1;
    if (!this.isSelf) {
      const response = this.service.GetAll(this.service.currentPage);
      response.subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.service.list = data.result as Todo[];
          this.service.pageCount = data.count;
        }
      }, error => { console.log(error); });

    } else {
      const response = this.service.GetAllAssign(this.service.currentPage);
      response.subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.service.list = data.result as Todo[];
          this.service.pageCount = data.count;
        }
      }, error => { console.log(error); });
    }
  }

  AssignTodo(todo: Todo) {
    const initialState = {
      todo: todo
    };
    this.bsModalRef = this.modalService.show(AssignTodoItemComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }
  EditTodo(id: number) {
    const initialState = {
      id: id
    };
    this.bsModalRef = this.modalService.show(EditTodoItemComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  DeleteTodo(id: number) {
    const initialState = {
      id: id
    };
    this.bsModalRef = this.modalService.show(DeleteTodoItemComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  CloseTodo(id: number) {
    const initialState = {
      id: id
    };
    this.bsModalRef = this.modalService.show(CloseTodoItemComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  Test(): void {
    console.log('here');
  }
  ChangePage(page: number) {
    this.service.currentPage = page;
    if (!this.isSelf) {
      const response = this.service.SearchAll(page);
      response.subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.service.list = data.result as Todo[];
          this.service.pageCount = data.count;
        }
      }, error => {
        console.log(error);
      });

    } else {
      const response = this.service.GetAllAssign(page);
      response.subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.service.list = data.result as Todo[];
          this.service.pageCount = data.count;
        }
      }, error => {
        console.log(error);
      });
    }
  }
}
