import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {StorageService} from '../../services/storage.service';
import {Injectable} from '@angular/core';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(private storage: StorageService) {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const modified = req.clone({setHeaders: {'X-XSRF-TOKEN': this.storage.getAntiToken()}});
    const modifiedA = modified.clone({setHeaders: {'Authorization': 'BEARER ' + this.storage.getToken()}});
    return next.handle(modifiedA);
  }

}
