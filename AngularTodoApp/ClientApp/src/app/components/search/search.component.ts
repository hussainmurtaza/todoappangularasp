import { Component, OnInit } from '@angular/core';
import {DecendingTodo, OrderByTodo, SearchTodo, StatusEnum, Todo} from '../../model/todo';
import {UtilComboBox} from '../../util/util-combo-box';
import {TodoService} from '../../services/todo.service';
import {ResponseResult} from '../../resultModel/responseResult';
import {UtilResponse} from '../../util/util-response';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  SearchText = '';
  Status = 0;
  OrderBy = 0;
  Search = 0;
  Decending = 0;
  StatusEnumData: UtilComboBox[] = [];
  DecendingEnumData: UtilComboBox[] = [];
  OrderByTodoData: UtilComboBox[] = [];
  SearchTodoData: UtilComboBox[] = [];
  constructor(private service: TodoService) { }

  ngOnInit() {
    Object.keys(StatusEnum).filter(key => !isNaN(Number(StatusEnum[key]))).forEach(p => {
      const d = new UtilComboBox();
      d.display = p;
      d.value = StatusEnum[p];
      this.StatusEnumData.push(d);
    });
    Object.keys(OrderByTodo).filter(key => !isNaN(Number(OrderByTodo[key]))).forEach(p => {
      const d = new UtilComboBox();
      d.display = p;
      d.value = OrderByTodo[p];
      this.OrderByTodoData.push(d);
    });
    Object.keys(SearchTodo).filter(key => !isNaN(Number(SearchTodo[key]))).forEach(p => {
      const d = new UtilComboBox();
      d.display = p;
      d.value = SearchTodo[p];
      this.SearchTodoData.push(d);
    });
    Object.keys(DecendingTodo).filter(key => !isNaN(Number(DecendingTodo[key]))).forEach(p => {
      const d = new UtilComboBox();
      d.display = p;
      d.value = DecendingTodo[p];
      this.DecendingEnumData.push(d);
    });

  }

  public SearchTask(): void {
    this.service.currentPage = 1;
    this.service.Search(this.service.currentPage, this.SearchText, this.Search, this.OrderBy, this.Status, this.Decending)
      .subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.service.clearList();
          this.service.list = this.service.list.concat(data.result as Todo[]);
          this.service.pageCount = data.count;
        }
      }, er => { console.log(er); });
  }


  onKeyPress(event: any) {

  }
}
