import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
})
export class PaginatorComponent implements OnInit {

  @Input() currentPage: number;
  @Input() TotalPage: number;
  @Output() pageClick = new EventEmitter<number>();
  constructor() {

  }

  ngOnInit() {
  }

  changePage(page: number) {
      this.pageClick.emit(page);
  }
}
