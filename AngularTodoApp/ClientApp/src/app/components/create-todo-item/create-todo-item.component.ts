import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StorageService} from '../../services/storage.service';
import {Todo} from '../../model/todo';
import {TodoService} from '../../services/todo.service';
import {ResponseResult} from '../../resultModel/responseResult';
import {UtilResponse} from '../../util/util-response';
import {Location} from '@angular/common';
import {AlertService} from '../../services/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-todo-item',
  templateUrl: './create-todo-item.component.html',
  styleUrls: ['./create-todo-item.component.css']
})
export class CreateTodoItemComponent implements OnInit {

  createForm: FormGroup;
  titleControl: FormControl;
  statusControl: FormControl;
  constructor(private storage: StorageService, private service: TodoService, private location: Location, private alert: AlertService, private router: Router) { }
  ngOnInit() {
    this.titleControl = new FormControl('', [Validators.required, Validators.pattern('^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$')]);
    this.statusControl = new FormControl('', [Validators.required, Validators.min(1), Validators.max(3)]);
    this.createForm = new FormGroup({
      title: this.titleControl,
      status: this.statusControl
    });
  }

  onSubmit(form: FormGroup) {
    if (form.valid) {
      const t = new Todo();
      t.title = this.titleControl.value;
      t.status = this.statusControl.value;
      this.service.Create(t).subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.alert.success('Item Created');
          this.titleControl.reset();
          this.statusControl.reset();
        } else if (data.message === UtilResponse.BAD || data.message === UtilResponse.ERROR) {
          data.error.map( p => {
            this.alert.warn(p);
          });
        }
      });
    }
  }

  goBackLocation() {
    this.location.back();
  }
}
