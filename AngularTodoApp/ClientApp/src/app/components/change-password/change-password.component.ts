import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ChangeValidator} from '../../validator/custom-validator';
import {ResponseResult} from '../../resultModel/responseResult';
import {UtilResponse} from '../../util/util-response';
import {UserService} from '../../services/user.service';
import {AlertService} from '../../services/alert.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit {

  changeForm: FormGroup;
  passwordFormGroup: FormGroup;
  newPassword: FormControl;
  password: FormControl;
  newRepeatPassword: FormControl;
  constructor(private service: UserService, private alert: AlertService) {

  }

  ngOnInit() {
    this.password = new FormControl('', [Validators.required, Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.newPassword = new FormControl('', [Validators.required, Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.newRepeatPassword = new FormControl('', [Validators.required, Validators.pattern(new RegExp('(?=^.{8,100}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*'))]);
    this.passwordFormGroup = new FormGroup({
      newPassword: this.newPassword,
      newRepeatPassword: this.newRepeatPassword
    }, ChangeValidator.validate.bind(this));
    this.changeForm = new FormGroup({
      password: this.password,
      passwordFormGroup: this.passwordFormGroup
    });
  }

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      const response = this.service.changePassword(this.password.value, this.newPassword.value, this.newRepeatPassword.value);
      response.subscribe(result => {
        const data = result as ResponseResult;
        if (data.message === UtilResponse.SUCCESS) {
          this.alert.success('Password Updated');
        } else if (data.message === UtilResponse.ERROR || data.message === UtilResponse.BAD) {
          data.error.forEach(p => {
            this.alert.warn(p);
          });
        }
      });
    }
  }
}
