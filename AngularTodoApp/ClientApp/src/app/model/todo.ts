import {Base} from './base';
import {User} from './user';

export class Todo extends Base {
  private _title: string;
  private _status: number;
  private _userId: string;
  private _assignToId: string;
  private _assignedUser: User;
  private _isClosed: boolean;
  private _closedDate: boolean;

  get isClosed(): boolean {
    return this._isClosed;
  }

  set isClosed(value: boolean) {
    this._isClosed = value;
  }

  get closedDate(): boolean {
    return this._closedDate;
  }

  set closedDate(value: boolean) {
    this._closedDate = value;
  }

  get assignToId(): string {
    return this._assignToId;
  }

  set assignToId(value: string) {
    this._assignToId = value;
  }

  get assignedUser(): User {
    return this._assignedUser;
  }

  set assignedUser(value: User) {
    this._assignedUser = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get status(): number {
    return this._status;
  }

  set status(value: number) {
    this._status = value;
  }

  get userId(): string {
    return this._userId;
  }

  set userId(value: string) {
    this._userId = value;
  }
}

export enum StatusEnum {
  Any = 0,
  Pending = 1,
  Completed = 2,
  InProgress = 3,
  Close = 4,
}

export enum OrderByTodo {
  None = 0,
  Status = 1,
  CreatedAt = 2,
  Name = 3,
  AssingedTo = 4,
}
export enum SearchTodo {
  Any = 0,
  Name = 1,
  CreadtedAt = 2,
  AssignedTo = 3,
}
export enum DecendingTodo {
  Ascending = 0,
  Descending = 1,
}
