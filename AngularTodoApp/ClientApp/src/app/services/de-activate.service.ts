import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {UserService} from './user.service';
import {state} from '@angular/animations';

@Injectable({
  providedIn: 'root'
})
export class DeActivateService implements CanDeactivate<any> {

  constructor(private service: UserService, private router: Router) { }

  canDeactivate(component: any, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.service.isAuthenticated()) {
      this.router.navigate(['login']);
      return true;
    } else {
      return false;
    }
  }
}
