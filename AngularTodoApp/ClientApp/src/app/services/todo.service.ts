import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {Todo} from '../model/todo';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Api} from '../api-route/api';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient, @Inject('BASE_URL')private  baseUrl: string) {
  }

  public list: Todo[];
  public currentPage: number;
  public pageCount: number;
  private searchText: string;
  private columnId: number;
  private orderBy: number;
  private status: number;
  private isDecending: number;
  public GetAll(page: number): Observable<any>  {
    const params = new HttpParams().set('page', page + '');
    return this.http.get(this.baseUrl + Api.TODO_GETALL, { params: params });
  }

  public Get(id: number): Observable<any>  {
    const params = new HttpParams().set('id', id + '');
    return this.http.get(this.baseUrl + Api.TODO_GET, { params: params });
  }

  public Create(todo: Todo): Observable<any> {
    const requestBody = {
      title: todo.title,
      status: todo.status,
      userId: todo.userId,
    };
    return this.http.post(this.baseUrl + Api.TODO_CREATE, requestBody);
  }

  public Delete(id: number): Observable<any> {
    const params = new HttpParams().set('id', id + '');
    return this.http.delete(this.baseUrl + Api.TODO_DELETE, { params: params });
  }

  public Update(todo: Todo): Observable<any> {
    const params = new HttpParams().set('id', todo.id + '');
    const requestBody = {
      title: todo.title,
      status: todo.status,
    };
    return this.http.put(this.baseUrl + Api.TODO_UPDATE, requestBody, { params: params});
  }

  public Assign(id: number, userId: string) {
    const params = new HttpParams().set('id', id + '');
    const requestBody = {
      assignUserId: userId
    };
    return this.http.put(this.baseUrl + Api.TODO_ASSIGN, requestBody, { params: params});

  }

  public Close(id: number) {
    const params = new HttpParams().set('id', id + '');
    return this.http.put(this.baseUrl + Api.TODO_CLOSE, null, { params: params});

  }

  public GetAllAssign(page: number) {
    const params = new HttpParams().set('page', page + '');
    return this.http.get(this.baseUrl + Api.TODO_GET_ALL_ASSIGN, { params: params });
  }

  public SearchAll(page: number) {
    if (this.searchText === undefined) {
      this.searchText = '';
    }
    const params = new HttpParams().set('page', page + '').set('search', this.searchText + '')
      .set('status', status + '').set('col', this.columnId + '').set('orderBy', this.orderBy + '').set('isDecending', this.isDecending + '');
    return this.http.get(this.baseUrl + Api.TODO_SEARCH, { params: params });
  }
  public Search(page: number, searchText: string, columnId: number, orderBy: number, status: number, isDecending: number) {
    if (searchText === undefined) {
      searchText = '';
    }
    this.searchText = searchText;
    this.columnId = columnId;
    this.orderBy = orderBy;
    this.status = status;
    this.isDecending = isDecending;
    const params = new HttpParams().set('page', page + '').set('search', searchText + '')
      .set('status', status + '').set('col', columnId + '').set('orderBy', orderBy + '').set('isDecending', isDecending + '');
    return this.http.get(this.baseUrl + Api.TODO_SEARCH, { params: params });
  }

  clearList() {
    this.list.length = 0;
  }
}
