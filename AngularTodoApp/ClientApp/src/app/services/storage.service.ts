import { Injectable } from '@angular/core';
import {Token} from '../resultModel/token';
import {Cookie} from 'ng2-cookies/ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class StorageService {


  private tokenKey = 'TokenKey';
  private tokenEndKey = 'TokenEndKey';
  private antiTokenKey = 'XSRF-TOKEN';
  private _userIdKey = 'userIdKey';

  setToken(value: Token): void {
    localStorage.setItem(this.tokenKey, value.token);
    localStorage.setItem(this.tokenEndKey, value.endDate + '');
  }
  get userId(): string {
    const d = localStorage.getItem(this._userIdKey);
  if (d != null) {
    return d;
  }
  return '';
  }

  set userId(value: string) {
    localStorage.setItem(this._userIdKey, value );
  }
  getExpired(): Date {
    if (localStorage.getItem(this.tokenEndKey).length === 0) {
      return null;
    }
    const d = new Date(localStorage.getItem(this.tokenEndKey));
    if (d != null) {
      return d;
    }
    return null;
  }
  getToken(): string {
    const d = localStorage.getItem(this.tokenKey);
    if (d != null) {
      return d;
    }
    return '';
  }
  getAntiToken(): string {
    const d = Cookie.get(this.antiTokenKey);
    if (d != null) {
      return d;
    }
    return '';
  }

  constructor() {

  }

  clear() {
    this.userId = '';
    localStorage.setItem(this.tokenKey, '');
    localStorage.setItem(this.tokenEndKey, '');
  }
}
