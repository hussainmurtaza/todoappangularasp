import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';
import {Api} from '../api-route/api';
import {StorageService} from './storage.service';
import {Observable} from 'rxjs/internal/Observable';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isFirst = false;
  public authenticated = false;
  public expiryDate: Date = null;
  constructor(private router: Router, private http: HttpClient, @Inject('BASE_URL')private  baseUrl: string, private storage: StorageService) {
  }

  public registerUser(user: User): Observable<any> {
    const requestBody = {
      fullName: user.fullName,
      email: user.email,
      password: user.password,
      confirmPassword: user.confirmPassword,
    };
    return this.http.post(this.baseUrl + Api.USER_CREATE,
      requestBody);
  }

  public GetAllUsers(): Observable<any> {
    return this.http.get(this.baseUrl + Api.USER_GET_ALL);
  }
  public loginUser(user: User, rememberMe: boolean = false): Observable<any> {
    const requestBody = {
      email: user.email,
      password: user.password,
      rememberMe: rememberMe,
    };
    return this.http.post(this.baseUrl + Api.USER_LOGIN,
      requestBody);
  }

  public forgetPassword(email: string) {
    const requestBody = {
      email: email,
    };
    return this.http.post(this.baseUrl + Api.USER_FORGET_PASSWORD,
      requestBody);
  }

  public resetPassword(email: string, code: string, userId: string, password: string, confirmPassword: string) {
    const requestBody = {
      email: email,
      code: code,
      userId: userId,
      password: password,
      confirmPassword: confirmPassword,
    };
    return this.http.post(this.baseUrl + Api.USER_RESET_PASSWORD,
      requestBody);
  }
  public changePassword(password: string, newPassword: string, confirmNewPassword: string) {
    const requestBody = {
      OldPassword: password,
      NewPassword: newPassword,
      ConfirmPassword: confirmNewPassword,
    };
    return this.http.post(this.baseUrl + Api.USER_CHANGE_PASSWORD,
      requestBody);
  }

  public isAuthenticated(): boolean {
      if (this.expiryDate === null) {
        this.expiryDate = this.storage.getExpired();
      }
      if (this.expiryDate != null && this.expiryDate >= new Date()) {
        this.isFirst = true;
        this.authenticated = true;
      } else {
        if (this.isFirst) {
          this.isFirst = false;
          this.router.navigateByUrl('/account/login').then(p => {
            console.log(p);
          }, er => {
            console.error(er);
          });
        }
        this.authenticated = false;
      }
    return this.authenticated;
  }
}
