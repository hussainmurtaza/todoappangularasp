export class ResponseResult {
  get result(): any {
    return this._result;
  }

  set result(value: any) {
    this._result = value;
  }

  private _result: any;

  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
  }
  private _message: string;
  private _error: string[];
  private _count: number;

  get error(): string[] {
    return this._error;
  }

  set error(value: string[]) {
    this._error = value;
  }

  get message(): string {
    return this._message;
  }

  set message(value: string) {
    this._message = value;
  }


}
