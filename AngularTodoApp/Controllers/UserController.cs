﻿using System;
using System.Diagnostics;
using System.Linq;
using AngularTodoApp.Context;
using AngularTodoApp.Extensions;
using AngularTodoApp.Util;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AngularTodoApp.Controllers
{
    [Produces("application/json")]
    [Route("api/User"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {
        private readonly DbContext _context;

        public UserController(DbContext context)
        {
            _context = context;
        }

        [HttpGet,Route("getAll")]
        public IActionResult GetAll()
        {
            try
            {
                var userId = User.UserId();
                var list = _context.Users.Where(p => p.Id != userId).ToList();
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                    result = list
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Ok(new
            {
                message = UtilResponse.SUCCESS,
                error = new[]
                {
                    "Error Occured"
                }
            });
        }
    }
}
