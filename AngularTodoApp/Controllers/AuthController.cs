﻿using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AngularTodoApp.Context;
using AngularTodoApp.Extensions;
using AngularTodoApp.Model;
using AngularTodoApp.Services;
using AngularTodoApp.Util;
using AngularTodoApp.ViewModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AngularTodoApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    [EnableCors("MyPolicy")]
    public class AuthController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly DbContext _context;
        private readonly SignInManager<User> _signInManager;

        public AuthController(UserManager<User> userManager, DbContext context, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _context = context;
            _signInManager = signInManager;
        }

        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel model, string returnUrl = null)
        {
            try
            {
                ViewData["ReturnUrl"] = returnUrl;
                if (ModelState.IsValid)
                {
                    var user = new User { UserName = model.Email, Email = model.Email, FullName = model.FullName };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                        await new EmailSender().SendEmailConfirmationAsync("technosoft0334@gmail.com", callbackUrl);
                        return Ok(new
                        {
                            message = UtilResponse.SUCCESS,
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            message = UtilResponse.BAD,
                            error = new []
                            {
                                "Account Already Exists"
                            }
                        });
                    }
                }
                return Ok(new
                {
                    message = UtilResponse.BAD,
                    error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Unauthorized();
        }

        [Route("test")]
        [HttpPost]
        public IActionResult Test()
        {
            return Ok(new
            {
                success = true
            });
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            try
            {


                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
                    {
                        var span = TimeSpan.FromMinutes(10);
                        if (model.RememberMe)
                        {
                            span = TimeSpan.FromDays(365);
                        }
                        return Ok(new
                        {
                            message = UtilResponse.SUCCESS,
                            result = new
                            {
                                token = JwtTokenUtil.Create(model.Email,
                                    new Claim[] {new Claim("FullName", user.FullName), new Claim("Id", user.Id), }, span),
                                endDate = DateTime.Now.Add(span)
                            }
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            message = UtilResponse.BAD,
                            error = new[]
                            {
                                "Invalid User/Password"
                            }
                        });
                    }
                }
                return Ok(new
                {
                    message = UtilResponse.BAD,
                    error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Unauthorized();
        }

        [HttpPost, Route("forgetPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return Ok(new
                    {
                        message = UtilResponse.BAD,
                        error = new[]
                        {
                            "Unable to Process"
                        }
                    });
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await new EmailSender().SendEmailAsync("technosoft0334@gmail.com", "Reset Password",
                    $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                });
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
            });
            // If we got this far, something failed, redisplay form
        }

        [Route("resetPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    message = UtilResponse.BAD,
                    error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
                });
                //return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return Ok(new
                {
                    message = UtilResponse.BAD,
                    error = new[]
                    {
                        "Unable to Process"
                    }
                });
                // Don't reveal that the user does not exist
                //eturn RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                });
                //return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = new[]
                {
                    "Unable to Process"
                }
            });
            //AddErrors(result);
            //return View();
        }


        [HttpPost,Route("changePassword")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new
                {
                    message = UtilResponse.BAD,
                    error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
                });
            }
            var userId = User.UserId();
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!changePasswordResult.Succeeded)
            {
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                    error = changePasswordResult.Errors.Select(p => p.Description)
                });
            }
            return Ok(new
            {
                message = UtilResponse.SUCCESS,
            });
        }


    }
}
