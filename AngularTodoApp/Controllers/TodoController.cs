﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AngularTodoApp.Context;
using AngularTodoApp.Extensions;
using AngularTodoApp.Model;
using AngularTodoApp.Repository;
using AngularTodoApp.Util;
using AngularTodoApp.ViewModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DbContext = AngularTodoApp.Context.DbContext;

namespace AngularTodoApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Todo"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TodoController : Controller
    {
        private readonly DbContext _context;

        public TodoController(DbContext context)
        {
            _context = context;
        }
        [Route("getAssign")]
        [HttpGet]
        public IActionResult GetAssign(int page = 1, int pageSize = 10)
        {
            try
            {
                var userId = User.UserId();
                var repo = new TodoItemRepository(_context);
                var list = repo.GetMany(p => p.AssignToId == userId, p => p.CreatedAt, page, pageSize, true, source => source.Include(p => p.AssignedUser)).ToList();
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                    result = list
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Unauthorized();
        }

        [Route("getAll")]
        [HttpGet]
        public IActionResult GetAll(int page = 1,int pageSize = 10)
        {
            try
            {
                var userId = User.UserId();
                var repo = new TodoItemRepository(_context);
                var list = repo.GetMany(p => p.UserId == userId, p => p.CreatedAt, page, pageSize, true, source => source.Include(p => p.AssignedUser)).ToList();
                var count = repo.PageCount(p => p.UserId == userId,pageSize);
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                    result = list,
                    count
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Unauthorized();
        }


        [Route("get")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            try
            {
                var userId = User.Claims.FirstOrDefault(p => p.Type == "Id")?.Value;
                var repo = new TodoItemRepository(_context);
                var list = repo.GetBy(p => p.Id == id && p.UserId == userId);
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                    result = list
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Unauthorized();
        }


        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]TodoItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.UserId();
                if (string.IsNullOrEmpty(userId))
                {
                    return Ok(new
                    {
                        message = UtilResponse.BAD,
                        error = new[] { "Error Occured"}
                    });
                }
                var todo = new Todo()
                {
                    UserId = userId,
                    CreatedAt = DateTime.Now,
                    Title = model.Title,
                    Status = model.Status,
                    UpdatedAt = DateTime.Now
                };
                _context.Add(todo);
                await _context.SaveChangesAsync();
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                });
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
            });
        }

        [Route("update")]
        [HttpPut]
        public async Task<IActionResult> Update(int id, [FromBody]TodoItemViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(new
                    {
                        message = UtilResponse.BAD,
                        error = ModelState.Values.SelectMany(p => p.Errors.Select(pr => pr.ErrorMessage))
                    });
                }
                var userId = User.Claims.FirstOrDefault(p => p.Type == "Id")?.Value;
                var repo = new TodoItemRepository(_context);
                var item = repo.GetBy(p => p.UserId == userId && id == p.Id);
                if (item != null)
                {
                    item.UpdatedAt = DateTime.Now;
                    item.Title = model.Title;
                    item.Status = model.Status;
                    repo.Update(item);
                    await repo.SaveChangesAsync();
                    return Ok(new
                    {
                        message = UtilResponse.SUCCESS,
                        result = item
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = new[]
                {
                    "Error Occurred"
                }
            });
        }


        [Route("assign")]
        [HttpPut]
        public async Task<IActionResult> Assign(int id, [FromBody]TodoItemAssignViewModel model)
        {

            try
            {
                if (string.IsNullOrEmpty(model.AssignUserId))
                {
                    return Ok(new
                    {
                        message = UtilResponse.BAD,
                        error = new[]
                        {
                            "No User Selected"
                        }
                    });
                }
                var userId = User.UserId();
                var repo = new TodoItemRepository(_context);
                var item = repo.GetBy(p => p.UserId == userId && id == p.Id);
                if (item != null)
                {
                    item.UpdatedAt = DateTime.Now;
                    item.AssignToId = model.AssignUserId;
                    repo.Update(item);
                    await repo.SaveChangesAsync();
                    return Ok(new
                    {
                        message = UtilResponse.SUCCESS,
                        result = item
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = new[]
                {
                    "Error Occurred"
                }
            });
        }


        [Route("close")]
        [HttpPut]
        public async Task<IActionResult> Close(int id)
        {

            try
            {
                var userId = User.UserId();
                var repo = new TodoItemRepository(_context);
                var item = repo.GetBy(p => p.UserId == userId && id == p.Id);
                if (item != null)
                {
                    item.UpdatedAt = DateTime.Now;
                    item.Status = 4;
                    item.IsClosed = true;
                    item.ClosedDate = DateTime.Now;
                    repo.Update(item);
                    await repo.SaveChangesAsync();
                    return Ok(new
                    {
                        message = UtilResponse.SUCCESS,
                        result = item
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = new[]
                {
                    "Error Occurred"
                }
            });
        }

        [Route("delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {

            try
            {
                var userId = User.Claims.FirstOrDefault(p => p.Type == "Id")?.Value;
                var repo = new TodoItemRepository(_context);
                if (repo.Delete(p => p.UserId == userId && p.Id == id))
                {
                    await repo.SaveChangesAsync();
                    return Ok(new
                    {
                        message = UtilResponse.SUCCESS,
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = new[]
                {
                    "Error Occurred"
                }
            });
        }


        [HttpGet, Route("search")]
        public IActionResult Search(int page = 1, string search = "", int col = 0, int status = 0, int orderBy = 0, int isDecending = 0)
        {
            try
            {
                var userId = User.UserId();
                var repo = new TodoItemRepository(_context);
                search = search ?? "";
                var list = repo.Search(userId, search.ToLower(), (SearchTodo) col, (StatusEnum) status, (OrderByTodo) orderBy,
                    page, 10, isDecending == 1, source => source.Include(p => p.AssignedUser)).ToList();
                var count = repo.SearchCount(userId, search.ToLower(), (SearchTodo)col, (StatusEnum)status);
                return Ok(new
                {
                    message = UtilResponse.SUCCESS,
                    result = list,
                    count
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return Ok(new
            {
                message = UtilResponse.BAD,
                error = new[]
                {
                    "Error Occured"
                }
            });
        }
    }
}

