﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace AngularTodoApp.Util
{
    public class JwtTokenUtil
    {
        public static string Secret = "";
        public static string Issuer = "";
        public static string Audience = "";
        public static string Create(string email)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,email),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
            };
            var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
            var token = new JwtSecurityToken(
                issuer: Issuer,
                audience: "http://www.google.com",
                expires: DateTime.UtcNow.AddDays(2),
                claims: claims,
                signingCredentials: new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256)
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        public static string Create(string email, Claim[] claimss, TimeSpan? timeSpanExpire = null)
        {
            if (timeSpanExpire == null)
            {
                timeSpanExpire = TimeSpan.FromDays(1);
            }
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            foreach (var claim in claimss)
            {
                claims.Add(claim);
            }
            var signingkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
            var token = new JwtSecurityToken(
                issuer: Issuer,
                audience: Audience,
                expires: DateTime.UtcNow.Add(timeSpanExpire.Value),
                claims: claims,
                signingCredentials: new SigningCredentials(signingkey, SecurityAlgorithms.HmacSha256)
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
