﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTodoApp.Util
{
    public class UtilResponse
    {
        public const string SUCCESS = "Success";
        public const string ERROR = "Error";
        public const string BAD = "Bad Request";
    }
}
